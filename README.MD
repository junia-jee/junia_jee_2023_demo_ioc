# IOC DEMO

In this demo, you will find 3 ways to do some IoC with Spring framework:
* The XML "old fashion" way inside the `junia.demo.ioc.apps.xmlversion` package
* The Java version with a configuration class, inside the `junia.demo.ioc.apps.javaversion` package
* The less flexible but very popular way with annotations inside the `junia.demo.ioc.apps.annotationversion`
