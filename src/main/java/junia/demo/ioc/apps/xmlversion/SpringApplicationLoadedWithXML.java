package junia.demo.ioc.apps.xmlversion;

import junia.demo.ioc.domain.drawing.Drawing;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringApplicationLoadedWithXML {

    public static void main(String[] args) {
        try (ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("META-INF/applicationContext.xml")) {
            Drawing drawing = context.getBean(Drawing.class);
            System.err.println(drawing.getTotalPerimeter());
            System.err.println(drawing.getTotalSurface());
        }
    }
}
