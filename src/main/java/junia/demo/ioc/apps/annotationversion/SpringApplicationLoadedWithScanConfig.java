package junia.demo.ioc.apps.annotationversion;

import junia.demo.ioc.apps.annotationversion.config.ScanConfig;
import junia.demo.ioc.domain.biz.ComponentA;
import junia.demo.ioc.domain.biz.ComponentB;
import junia.demo.ioc.domain.biz.ComponentC;
import junia.demo.ioc.domain.biz.ComponentD;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


public class SpringApplicationLoadedWithScanConfig {

    public static void main(String[] args) {
        try(AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ScanConfig.class)) {
            ComponentA componentA = context.getBean(ComponentA.class);
            ComponentB componentB = context.getBean(ComponentB.class);
            ComponentC componentC = context.getBean(ComponentC.class);
            ComponentD componentD = context.getBean(ComponentD.class);
            System.err.println("Component A is not null : " + (componentA != null ? "true" : "false"));
            System.err.println("Component B is not null : " + (componentB != null ? "true" : "false"));
            System.err.println("Component C is not null : " + (componentC != null ? "true" : "false"));
            System.err.println("Component D is not null : " + (componentD != null ? "true" : "false"));
            System.err.println("Child of Component A is Component B : " + (componentA.getComponentB()==componentB?"true":"false"));
            System.err.println("Child 1 of Component B is Component C : " + (componentB.getComponentC()==componentC?"true":"false"));
            System.err.println("Child 2 of Component B is Component D : " + (componentB.getComponentD()==componentD?"true":"false"));
        }
    }
}
