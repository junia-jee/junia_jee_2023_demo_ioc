package junia.demo.ioc.apps.annotationversion.config;

import junia.demo.ioc.domain.ddd.BusinessService;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScan(
        basePackages = "junia.demo.ioc.domain.biz",
        includeFilters = @ComponentScan.Filter(type = FilterType.ANNOTATION, classes = BusinessService.class))
public class ScanConfig {

}
