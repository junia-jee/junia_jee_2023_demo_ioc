package junia.demo.ioc.apps.diexperiment;

import junia.demo.ioc.apps.annotationversion.config.ScanConfig;
import junia.demo.ioc.domain.ddd.BusinessService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScan(
        basePackages = "junia.demo.ioc.apps.diexperiment",
        includeFilters = @ComponentScan.Filter(type = FilterType.ANNOTATION, classes = BusinessService.class))
public class DependencyInjectionExperiment {


    public static void main(String[] args) {
        try (AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DependencyInjectionExperiment.class)) {
            ServiceA ServiceA = context.getBean(ServiceA.class);
            ServiceB ServiceB = context.getBean(ServiceB.class);
            ServiceC ServiceC = context.getBean(ServiceC.class);
            System.err.println(ServiceA);
            System.err.println(ServiceB);
            System.err.println(ServiceC);
        }
    }


}
