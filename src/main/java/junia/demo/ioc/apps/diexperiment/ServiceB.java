package junia.demo.ioc.apps.diexperiment;

import jakarta.inject.Inject;
import junia.demo.ioc.domain.ddd.BusinessService;

@BusinessService
public class ServiceB {

    @Inject
    private ServiceB serviceB;

}
