package junia.demo.ioc.domain.drawing;

import junia.demo.ioc.domain.shape.Shape;

import java.util.List;

public class Drawing {

    private final List<Shape> shapes;


    public Drawing(List<Shape> shapes) {
        super();
        this.shapes = shapes;
    }


    public double getTotalPerimeter() {
        return shapes.stream().mapToDouble(Shape::getPerimeter).sum();
    }


    public double getTotalSurface() {
        return shapes.stream().mapToDouble(Shape::getSurface).sum();
    }



}